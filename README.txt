Signup Rules module - README

The Signup Rules module provides rules integration for the Signup
module.

1) Features of Signup Rules for Drupal 6

1.1) Rules events

a) User signed up to content
   After a user has signed up to content this event is triggered.
   The arguments "Signup data", "Content to which the user signed up"
   and "Acting user" are provided to conditions and actions triggered
   by this event.
   There are a lot of possible usages for this event, e.g. displaying
   additional messages for signup or granting user points (use
   userpoints module - http://drupal.org/projects/userpoints).

b) User updated the signup to content
   After a user has changed the settings in te signup form this event
   is triggered. The arguments "Signup data", "Content to which the
   user signed up" and "Acting user" are provided to conditions and
   actions triggered by this event.
   Possible usages of event a) also apply to this event.

c) User canceled the signup to content
   After a user has canceled the signup to content this event is
   triggered. The arguments "Signup data", "Content to which the user
   signed up" and "Acting user" are provided to conditions and actions
   triggered by this event.
   Possible usages of event a) also apply to this event.

d) Signup to content was opened
   This event is triggered, after signup to a content was (re)opened.
   The argument "Content" is provided to conditions and actions
   triggered by this event.
   A signup is (re)opened when a signup is canceled, reducing the
   number of signups below the signup limit (unless the optional
   signup closing hour has passed), after a signup admin change to the
   signup limit or after an admin setting the signup status from
   "Closed" to "Open".
   The event is not triggered with the creation or updating of signup
   enabled content.

e) Signup to content was closed
   This event is triggered, after signup to a content was closed. The
   argument "Content" is provided to conditions and actions triggered
   by this event.
   Closing a signup happens when the signup limit has been reached,
   after a signup admin change to the signup limit, after a signup
   admin setting the signup status from "Open" to "Closed", or when
   cron finds that the optional closing hour for the signup has
   passed.
   The event is not triggered with the deletion of signup enabled
   content.

f) Display of signup form is being inquired
   This event is triggered before the display of a signup form, if the
   acting user has permission to signup up to content.
   Signup form display actions are available to forbid or allow
   displaying the signup form to acting users managing their own
   signups. If more than one display action is called, the action with
   the highest weight on the triggered rule with the highest weight
   hits. If no display action is called, the default, "Allow displaying
   signup form", applies.
   This event cannot be used to prevent permitted users from managing
   signups of other users.

   Usage: One possible use case is to prevent users with signup
          permission to sign up themselves to signup enabled content
          based on configured conditions. A project requirement might
          be, that a user needs to have a minimum of userpoints to be
          allowed to signup, etc.
          For such use case the non-administrative users may have the
          signup permissions "signup for content", "edit own signup"
          and "cancel own signup", but not the permissions "administer
          all signups", "administer signups for own content" and
          "cancel signups", otherwise the users could sign up
          themselves via other means than the signup form.

1.2) Rules conditions

a) Is user signed up to content
   This condition checks whether a user is signed up to content. It
   requires the arguments "User" and "Content". "User" is always
   available, should "Content" not be offered or being loaded, this
   condition is not configurable.
   In case the condition is used with non-signup enabled content in the
   argument, it will always result to FALSE.

b) Is display of signup form allowed
   This condition checks the current display status of the signup form
   on signup enabled content. It requires the argument "Signup form
   display status" which is provided by the event "Display of signup
   form is being inquired". Other events will not offer this condition
   to be configured.
   The display status of the signup form can be changed by the actions
   "Allow displaying signup form" and "Forbid displaying signup form"
   (see also rules event "Display of signup form is being inquired" and
   the above rules actions).

c) Is signup status open for content
   This condition checks whether the signup status of content is open.
   It requires the argument "Content", otherwise the condition will
   not be configurable.
   In case the condition is used with non-signup enabled content in the
   argument, it will always result to FALSE.

1.3) Rules actions

a) Allow displaying signup form
   This action sets the signup form display status to allowed. It is only
   useful, if the status might have been set to forbidden by an earlier
   action as the default of the display status is "allowed".
   This action requires the argument "Signup form display status" which
   is provided by the event "Display of signup form is being inquired".
   Other events will not offer this action to be configured.

b) Forbid displaying signup form
   This action sets the signup form display status to forbidden. It
   requires the argument "Signup form display status" which is provided
   by the event "Display of signup form is being inquired".
   Other events will not offer this action to be configured.

1.4) Future

   In subsequent versions additional rules events, conditions and actions
   according to project needs and/or feature requests will be added.


2) Dependencies

   Rules 1.x module (http://drupal.org/project/rules)
   Signup 1.x module (http://drupal.org/project/signup)


3) Installation

a) Have the latest version of Drupal 6, Signup 1.x and Rules 1.x installed
   and acivated according to the installation instructions in the respective
   installation packages.

b) Untar or unzip the contents of the Signup Rules package into the Drupal
   module directory (/sites/all/modules/ or /sites/<yourdomain.name>/modules/
   according to whether you have one or more domains running on the same
   Drupal installation.

c) Activate the module by checking the checkbox at "Signup Rules" on the
   module list admin page (http://yoursite.tld/admin/build/modules)

d) Start building your triggered rules on signup rules events.


4) Update

   Updating the module from a previous version is done according to the
   standard procedure.

a) Log into your website as user 1.

b) Set your site into offline mode.

c) Remove the old version of the module from your modules directory.

d) Copy the new module into the modules directory.

e) Go to the upgrade page http://<yourdomain.name>/upgrade.php and follow
   the directions on the page.

f) Set your site into online mode.


4) Uninstalling Signup Rules

a) Just uncheck the checkbox at "Signup Rules". No further uninstallation
   is necessary.   


5) Future of Signup Rules, Issues and Thanks to Contributors

   In subsequent versions of the module further rules events and new rules
   conditions and actions are planned. Please have a regular look at the
   module project page (http://drupal.org/project/signup_rules).

   Send feature requests and bug reports to the issue tracking system for
   the signup rules module: http://drupal.org/node/add/project-issue/signup_rules.

   The module is maintained by meichr (http://drupal.org/user/256793).


6) References

   The idea and parts of the code of Signup Rules comes from the signup issue
   "Add rules event for signup" (http://drupal.org/node/595414) and was
   modified to be in its own module using the signup api in addition to the rules
   api. Thanks to shenzhuxi for starting the issue and Magnus for the patch on
   comment #20.

