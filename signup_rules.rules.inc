<?php

/**
 * @file
 * Provides rules integration for the signup module.
 */

/**
 * Implementation of hook_rules_event_info().
 * @ingroup rules
 */
function signup_rules_rules_event_info() {
  return array(
    'signup_rules_insert' => array(
      'label' => t('User signed up to content'),
      'module' => 'Signup',
      'arguments' => signup_rules_events_signup_arguments(FALSE),
      'help' => 'This event is triggered, after the user has signed '.
                'up or was signed up to content.',
    ),
    'signup_rules_update' => array(
      'label' => t('User updated signup to content'),
      'module' => 'Signup',
      'arguments' => signup_rules_events_signup_arguments(FALSE),
      'help' => 'This event is triggered, after the user has '.
                'updated a signup to content.',
    ),
    'signup_rules_cancel' => array(
      'label' => t('User canceled signup to content'),
      'module' => 'Signup',
      'arguments' => signup_rules_events_signup_arguments(TRUE),
      'help' => 'This event is triggered, after the signup of a user '.
                'to content was canceled.',
    ),
    'signup_rules_open' => array(
      'label' => t('Signup to content was opened'),
      'module' => 'Signup',
      'arguments' => array(
        'node' => array('type' => 'node', 'label' => t('content')),
      ),
      'help' => 'This event is triggered, when signup to a content '.
                'was (re)opened. This can happen with a signup '.
                'cancellation after the signup limit had been reached '.
                '(unless the optional signup closing hour has passed), '.
                'after a signup admin change to the signup limit or '.
                'after an admin setting the signup status from "Closed" '.
                'to "Open". The event is not triggered with the creation or '.
                'update of signup enabled content.',
    ),
    'signup_rules_close' => array(
      'label' => t('Signup to content was closed'),
      'module' => 'Signup',
      'arguments' => array(
        'node' => array('type' => 'node', 'label' => t('content')),
      ),
      'help' => 'This event is triggered, when signup to a content '.
                'was closed. This can happen with a signup to content '.
                'reaching the signup limit, after a signup admin change '.
                'to the signup limit, after a signup admin setting the '.
                'signup status from "Open" to "Closed", or cron finds '.
                'that the optional closing hour for the signup has passed. '.
                'The event is not triggered with the deletion of signup '.
                'enabled content.',
    ),
    'signup_rules_suppress' => array(
      'label' => t('Display of signup form is being inquired'),
      'module' => 'Signup',
      'arguments' => array(
        'signdisp' => array('type' => 'signdisp', 'label' => t('signup form display status')),
        'node' => array('type' => 'node', 'label' => t('content')),
        'user' => array('type' => 'user', 'label' => t('acting user')),
      ),
      'help' => 'This event is triggered before the display of a signup form, '.
                'if the acting user has permission to signup up to content.<br />'.
                'Signup form display actions are available to forbid or allow '.
                'displaying the signup form to acting users managing their '.
                'own signups.<br />'.
                'If more than one display action is called, the action with '.
                'the highest weight on the triggered rule with the highest '.
                'weight hits. If no display action is called, the default, '.
                '\'Allow displaying signup form\', applies.<br />'.
                'This event is not used to prevent permitted users from managing '.
                'signups of other users.',
    ),
  );
}

/**
 * Default arguments to signup rules events.
 */
function signup_rules_events_signup_arguments($have_node = FALSE) {
  return array(
    'signup' => array(
      'type' => 'signup',
      'label' => t('signup data'),
    ),
    'node' => array(
      'type' => 'node',
      'label' => t('content to which the user signed up'),
      'handler' => $have_node ? '' : 'signup_rules_events_argument_signup_node',
    ),
    'account' => array(
      'type' => 'user',
      'label' => t('acting user'),
      'handler' => 'signup_rules_events_argument_signup_user',
    ),
  );
}

/**
 * Load associated node for a signup object.
 */
function signup_rules_events_argument_signup_node($signup) {
  return node_load($signup->nid);
}

/**
 * Load associated user account for a signup object.
 */
function signup_rules_events_argument_signup_user($signup) {
  // If signup user is anonymous user get basic info from signup object.
  if (!$signup->uid) {
    $account = new stdClass();
    $account->uid = 0;
    $account->name = '';
    $account->mail = $signup->anon_mail;
    $account->roles = array(DRUPAL_ANONYMOUS_RID => 'anonymous user');
  }
  else {
    $account = user_load($signup->uid);
  }
  return $account;
}

/**
 * Implementation of hook_rules_action_info().
 * @ingroup rules
 */
function signup_rules_action_info() {
  return array(
    'signup_rules_action_forbid_signup_form' => array(
      'label' => t('Forbid displaying signup form'),
      'module' => 'Signup',
      'arguments' => array(
        'signdisp' => array('type' => 'signdisp', 'label' => t('Signup form display status')),
      ),
    ),
    'signup_rules_action_allow_signup_form' => array(
      'label' => t('Allow displaying signup form'),
      'module' => 'Signup',
      'arguments' => array(
        'signdisp' => array('type' => 'signdisp', 'label' => t('Signup form display status')),
      ),
    ),
  );
}

/**
 * Disable the display of the signup form
 */
function signup_rules_action_forbid_signup_form($signdisp) {
  $signdisp = TRUE;
}

/**
 * Enable the display of the signup form
 */
function signup_rules_action_allow_signup_form($signdisp) {
  $signdisp = FALSE;
}

/**
 * Implementation of hook_rules_condition_info().
 * @ingroup rules
 */
function signup_rules_condition_info() {
  return array(
    'signup_rules_condition_is_user_signed_up' => array(
      'label' => t('Is user signed up to content'),
      'arguments' => array(
        'node' => array('type' => 'node', 'label' => t('Content')),
        'user' => array('type' => 'user', 'label' => t('Acting user')),
      ),
      'module' => 'Signup',
    ),
    'signup_rules_condition_is_signup_form_allowed' => array(
      'label' => t('Is display of signup form allowed'),
      'arguments' => array(
        'signdisp' => array('type' => 'signdisp', 'label' => t('Signup form display status')),
      ),
      'module' => 'Signup',
    ),
    'signup_rules_condition_is_signup_status_open' => array(
      'label' => t('Is signup status open for content'),
      'arguments' => array(
        'node' => array('type' => 'node', 'label' => t('Content')),
      ),
      'module' => 'Signup',
    ),
  );
}

/**
 * Condition Implementation: Is user signed up for node
 */
function signup_rules_condition_is_user_signed_up($node, $user) {

  $result = db_query("SELECT sl.uid, u.name FROM {signup_log} sl INNER JOIN {users} u ON sl.uid = u.uid WHERE sl.uid = %d AND sl.nid = %d", $user->uid, $node->nid);
  $account = db_fetch_object($result);
  if (!empty($account)) {
    return TRUE;
  }
  return FALSE;
}

/**
 * Condition Implementation: Is display of signup form allowed
 */
function signup_rules_condition_is_signup_form_allowed($signdisp) {
  return !$signdisp;
}

/**
 * Condition Implementation: Is signup status open for content
 */
function signup_rules_condition_is_signup_status_open($node) {
  return $node->signup_status == 1;
}

/**
 * Implementation of hook_rules_data_type_info().
 * @ingroup rules
 *
 * signdisp
 * Stores signup form display status (boolean) while triggered
 * rules for event "signup_supress" are being called.
 * There is only one signdisp variable per event, but setting
 * identifiable=TRUE is necessary to hide conditions and
 * actions for other events as they don't make sense for
 * other events.
 * Default is always "Permit Signup", if no permission
 * action is called.
 */
function signup_rules_data_type_info() {
  return array(
    'signup' => array(
      'label' => t('Signup data'),
      'class' => 'rules_data_type_signup',
      'savable' => FALSE,
      'identifiable' => TRUE,
    ),
    'signdisp' => array(
      'label' => t('Signup form display status'),
      'class' => 'rules_data_type',
      'savable' => FALSE,
      'identifiable' => TRUE,
      'uses_input_form' => FALSE,
      'module' => 'Signup',
    ),
  );
}

/**
 * Implements the signup data type.
 * @ingroup rules
 */
class rules_data_type_signup extends rules_data_type {
  function load($sid) {
    return signup_load_signup($sid);
  }

  function get_identifier() {
    $signup = $this->get();
    return $signup->sid;
  }
}
